import { Component } from '@angular/core';
import { Stepcounter } from 'ionic-native';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  supported:boolean = false;
  steps = 0;
  msg:string = "Checking for step count sensor...";

  constructor() {
    
  }

  ionViewDidLoad() {
      this.ready().then(result => this.init(result));
  }

  ready():Promise<boolean> {
    return new Promise<boolean>((resolve)=>{
      Stepcounter.deviceCanCountSteps().then(can => resolve(can), f=> resolve(false));
    })
  }

  init(can){
    this.supported = can;
    this.msg = can
        ? "Your today's step count:"
        :"Your device does not have Step Count Sensor"

    if(this.supported) {
      this.getOffset().then(offset => this.start(offset));
    }
  }

  start(offset:number) {
    this.steps = offset;
    Stepcounter.start(offset).then(
        onSuccess => this.updateSteps(),
        onFailure => console.log('stepcounter-start error', onFailure)
    );
  }

  getOffset():Promise<number>{
    return new Promise<number>((resolve)=>{
      Stepcounter.getTodayStepCount().then(count => resolve(count > 0 ? count: 0), f=> resolve(0));
    })
  }

  updateSteps() {
    Stepcounter.getStepCount().then(
        count =>{
          this.steps = count;
          setTimeout(()=> this.updateSteps(), 5000);
        }
    )
  }
}
